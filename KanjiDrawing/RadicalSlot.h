//
//  RadicalSlot.h
//  JapaneseMyWay
//
//  Created by Ray on 6/4/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KanjiRadical.h"
#import "Rect2.h"
#import "KanjiDrawingView.h"

@interface RadicalSlot : UILabel {
    KanjiRadical *expectedRadical;
    KanjiRadical *filledRadical;
    KanjiRadical *temporaryRadical;
    BOOL drawFrame;
    BOOL drawText;
}

@property (nonatomic, retain) KanjiRadical *expectedRadical;
@property (nonatomic, retain) KanjiRadical *filledRadical;
@property (nonatomic, retain) KanjiRadical *temporaryRadical;
@property (nonatomic, assign) BOOL drawFrame;
@property (nonatomic, assign) BOOL drawText;

-(id)initWithFrame:(CGRect)frame expectedRadical:(KanjiRadical*)inExpectedRadical;

@end
