//
//  WriteKanjiViewControllerDelegate.h
//  JapaneseMyWay
//
//  Created by Ray Price on 6/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef JapaneseMyWay_WriteKanjiViewControllerDelegate_h
#define JapaneseMyWay_WriteKanjiViewControllerDelegate_h

#import "WriteKanjiViewController.h"

@protocol WriteKanjiViewControllerDelegate

-(void)writeKanjiSkipped:(id)sender;
-(void)writeKanjiDrawn:(id)sender withScore:(float)inScore hadToLook:(BOOL)inHadToLook;
-(void)hadToLookWhileDrawingKanji:(id)sender;

@optional

-(void)comparisonDrawingTouched:(id)sender;

@end

#endif
