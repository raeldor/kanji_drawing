//
//  KanjiDrawingView.m
//  JapaneseMyWay
//
//  Created By Ray on 3/9/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "KanjiDrawingView.h"
#import "Vector2.h"
#import "KanjiStroke.h"
#import "GraphicsHelpers.h"
#import "KanjiRadical.h"

@implementation KanjiDrawingView

@synthesize drawing;
@synthesize quantizedDrawing;
@synthesize playTimer;
@synthesize isPlaying;
@synthesize isStatic;
@synthesize isDrawing;
@synthesize delegate;
@synthesize drawColor;
@synthesize shadowColor;
@synthesize drawOutline;
@synthesize strokeWidth;
@synthesize keepAspect;
@synthesize drawFrame;
@synthesize partOfDrawing;
@synthesize playFrame;

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        //  all view did load if we're creating manually
        [self initialize];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self initialize];
    }
    return self;
}

-(void)initialize {
	// Initialization code
	drawing = [[KanjiDrawing alloc] init];
    
    // support dark mode in ios13
    if (@available(iOS 13.0, *)) {
        self.backgroundColor = [[UIColor systemBackgroundColor] colorWithAlphaComponent:0.0f];
        self.drawColor = [UIColor labelColor];
        self.shadowColor = [UIColor lightGrayColor];
    } else {
        // Fallback on earlier versions
        self.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.0f];
        self.drawColor = [UIColor whiteColor];
        self.shadowColor = [UIColor lightGrayColor];
    }
    self.keepAspect = YES;
    self.drawFrame = NO;
    self.strokeWidth = 8;
    
    // set play frame to 9999 by default, so everything gets drawn
    playFrame = 9999;
}

-(void)refreshDrawing {
	[self setNeedsDisplay];
}

-(NSUInteger)getStrokeCount {
	return [drawing.strokes count];
}

-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event {
	NSSet *myTouches = [event touchesForView:self];
	UITouch *touch = [myTouches anyObject];
	CGPoint point = [touch locationInView:self];
    
	// cannot do this if we are playing
	if (!isPlaying && drawingEnabled) {
		// save last point to calc vector from position
		previousTouchPoint = point;
	}
}

-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    // don't cancel because is gesture
    [self touchesEnded:touches withEvent:event];
}

-(void)touchesMoved:(NSSet*)touches withEvent:(UIEvent*)event {
	NSSet *myTouches = [event touchesForView:self];
	UITouch *touch = [myTouches anyObject];
	CGPoint point = [touch locationInView:self];
	
	// cannot do this if we are playing
	if (!isPlaying && drawingEnabled) {
        // if we're adding the first point, we've started drawing
        if (!isDrawing) {
            // start drawing
            isDrawing = YES;
            
            // add new stroke
            Vector2 *newVector = [[Vector2 alloc] initWithX:previousTouchPoint.x y:previousTouchPoint.y];
            KanjiStroke *newStroke = [[KanjiStroke alloc] initWithPoint:newVector];
            [drawing.strokes addObject:newStroke];
            [newStroke release];
            [newVector release];
        }
        
		// add point to list
		Vector2 *newVector = [[Vector2 alloc] initWithX:point.x-previousTouchPoint.x y:point.y-previousTouchPoint.y];
		[[[drawing.strokes objectAtIndex:[drawing.strokes count]-1] vectors] addObject:newVector];
		[newVector release];
		[self setNeedsDisplay];
		previousTouchPoint = point;
	}
}

-(void)enableDrawing {
	drawingEnabled = YES;
}

-(void)disableDrawing {
	drawingEnabled = NO;
}

-(void)clearStrokes {
	// clear drawing
	[drawing clearStrokes];
	[self refreshDrawing];
    
    // remove quantized drawing too, otherwise the old version gets left behind
    // this was causing old drawing to be shown in gray when new draw was failing
    self.quantizedDrawing = nil;
}

-(void)touchesEnded:(NSSet*)touches withEvent:(UIEvent*)event {
	// if stroke has no vectors, remove
	if (isDrawing && drawingEnabled) {
		if ([[[drawing.strokes objectAtIndex:[drawing.strokes count]-1] vectors] count] == 0)
			[drawing.strokes removeObjectAtIndex:[drawing.strokes count]-1];
		else
			// send a message to the parent controller that stroke has finished
			[delegate didEndDrawingStroke:self];
		
		// reset drawing flag
		isDrawing = NO;
	}
}

-(void)initPlay  {
	// initialize drawing
    playFrame = 0;
}

-(void)startPlay {
    // set initial frame and set playing flag
    playFrame = 0;
	isPlaying = YES;
    
	// start play timer
	playTimer = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(playTimerFired:) userInfo:nil repeats:YES];
	
	// refresh display
	[self setNeedsDisplay];
}

-(void)stopPlay {
	// stop playing and refresh display
    if (isPlaying) {
        // invalidate timer
        isPlaying = NO;
        [playTimer invalidate];
        playTimer = nil;
        [self setNeedsDisplay];
        
        // set playframe to end so everything gets drawn next time
        playFrame = 9999;
    }
}

-(void)playTimerFired:(NSTimer*)inTimer {
    // increment play frame
    playFrame++;
    if (playFrame > [self getFrameCount]) {
        // invalidate the timer if we've finished
        isPlaying = NO;
        [inTimer invalidate];
        inTimer = nil;
        return;
    }
    
	// refresh display
	[self setNeedsDisplay];
}

-(int)getFrameCount {
    // count frames
    int frameCount = 0;
    for (int s=0; s < quantizedDrawing.strokes.count; s++) {
        KanjiStroke *thisStroke = [quantizedDrawing.strokes objectAtIndex:s];
        frameCount += thisStroke.vectors.count;
    }
    return frameCount;
}

-(void)drawKanjiOnContext:(CGContextRef)context withMargin:(float)inMargin toFrame:(int)inFrame {
	NSArray *drawStrokes;
    
    // if this is static, make or get quantized version
    if (isStatic) {
        // quantize if it hasn't been already or if stroke count has changed
        if (quantizedDrawing == nil || drawing.strokes.count != quantizedDrawing.strokes.count)
            quantizedDrawing = [[KanjiDrawing alloc] initWithStrokes:[drawing getQuantizedStrokes]];
        
        // get strokes for drawing in this frame
        if (partOfDrawing != nil)
            drawStrokes = [quantizedDrawing getStrokesForFrame:self.bounds asPartOfDrawing:partOfDrawing withMargin:inMargin];
        else
            drawStrokes = [quantizedDrawing getStrokesForFrame:self.bounds asPartOfDrawing:quantizedDrawing withMargin:inMargin];
    }
    else
        drawStrokes = drawing.strokes;
    
    // if no strokes, do nothing
    if (drawStrokes.count == 0)
        return;
    
	// loop through strokes
    int currentFrame = 0;
	for (int s=0; s < drawStrokes.count && currentFrame < inFrame; s++)
	{
		// move to start point
		KanjiStroke *stroke = [drawStrokes objectAtIndex:s];
		CGPoint pos = CGPointMake(stroke.startPosition.x, stroke.startPosition.y);
		CGContextMoveToPoint(context, pos.x, pos.y);
		CGPoint lastPoint = pos;
		
		// loop through points
        for (int p=0; p < stroke.vectors.count && currentFrame < inFrame; p++)
		{
			// draw to vector
			Vector2	*vector = [stroke.vectors objectAtIndex:p];
			CGPoint pos2 = CGPointMake(vector.x, vector.y);
            CGPoint addPos = CGPointMake(lastPoint.x+pos2.x, lastPoint.y+pos2.y);
			CGContextAddLineToPoint(context, addPos.x, addPos.y);
			lastPoint.x += pos2.x;
			lastPoint.y += pos2.y;
            
            // increment current frame
            currentFrame++;
		}
		CGContextStrokePath(context);
	}
}

- (void)drawRect:(CGRect)rect {
	// get graphics context
	CGContextRef context = UIGraphicsGetCurrentContext();
    
    // save state first
    CGContextSaveGState(context);
    
	// if we have a drawing
	if (drawing != nil)
	{
		// thicker strokes for iPad
        float realStrokeWidth = strokeWidth;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 30200
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            realStrokeWidth = realStrokeWidth * 2.0f;
#endif
		
		// configure line drawing options
		CGContextSetLineWidth(context, strokeWidth);
		CGContextSetLineCap(context, kCGLineCapRound);
		CGContextSetLineJoin(context, kCGLineJoinRound);
        
		// draw outline
		if (drawOutline) {
			// draw full stroke without shadow
			CGContextSetLineWidth(context, realStrokeWidth);
			CGContextSetStrokeColorWithColor(context, drawColor.CGColor);
			CGContextSetBlendMode(context, kCGBlendModeNormal);
            [self drawKanjiOnContext:context withMargin:realStrokeWidth toFrame:playFrame];
			
			// now draw again over the top to blank out center
			CGContextSetLineWidth(context, realStrokeWidth*0.8f);
			CGContextSetLineCap(context, kCGLineCapRound);
            CGContextSetStrokeColorWithColor(context, self.backgroundColor.CGColor);
//            CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
			CGContextSetBlendMode(context, kCGBlendModeClear);
            [self drawKanjiOnContext:context withMargin:realStrokeWidth toFrame:playFrame];
		}
		else {
			// draw actual stroke
			CGContextSetShadowWithColor(context, CGSizeMake(-2, 2), 0, shadowColor.CGColor);
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 30200
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
				CGContextSetShadowWithColor(context, CGSizeMake(-4, 4), 0, shadowColor.CGColor);
#endif
			CGContextSetLineWidth(context, realStrokeWidth);
			CGContextSetStrokeColorWithColor(context, drawColor.CGColor);
            // support dark mode
            if (@available(iOS 13.0, *)) {
                if (CGColorEqualToColor(drawColor.CGColor, UIColor.whiteColor.CGColor))
                    CGContextSetBlendMode(context, kCGBlendModeScreen);
                else {
                    if (CGColorEqualToColor(drawColor.CGColor, UIColor.blackColor.CGColor))
                        CGContextSetBlendMode(context, kCGBlendModeMultiply);
                    else
                        CGContextSetBlendMode(context, kCGBlendModeNormal);
                }
            } else {
                if (drawColor == [UIColor whiteColor])
                    CGContextSetBlendMode(context, kCGBlendModeScreen);
                else {
                    if (drawColor == [UIColor blackColor])
                        CGContextSetBlendMode(context, kCGBlendModeMultiply);
                    else
                        CGContextSetBlendMode(context, kCGBlendModeNormal);
                }
            }
            [self drawKanjiOnContext:context withMargin:realStrokeWidth toFrame:playFrame];
		}
        
        // reset context scaling etc
        CGContextRestoreGState(context);
	}
}

-(void)setFrame:(CGRect)frame {
	// call super
	[super setFrame:frame];
    
    // redraw because of stroke width
    [self setNeedsDisplay];
}

- (void)dealloc {
	// stop playing if drawing
	if (isPlaying)
		[self stopPlay];
	[drawColor release];
	[drawing release];
    [partOfDrawing release];
	[quantizedDrawing release];
    [super dealloc];
}


@end
