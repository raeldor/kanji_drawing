/*
 *  KanjiDrawingViewDelegate.h
 *  JapaneseMyWay
 *
 *  Created by Ray on 5/24/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

@protocol KanjiDrawingViewDelegate

@optional

-(void)didEndDrawingStroke:(id)inDrawingView;

@end
