//
//  RadicalDrawingViewDelegate.h
//  JapaneseMyWay
//
//  Created by Ray on 6/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "KanjiRadical.h"

@protocol RadicalDrawingViewDelegate

@optional

-(void)didMoveRadical:(KanjiRadical*)inRadical toPoint:(CGPoint)inPoint;
-(BOOL)didLetGoOfRadical:(KanjiRadical*)inRadical atPoint:(CGPoint)inPoint;

@end