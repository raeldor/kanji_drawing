//
//  KanjiDrawingView.h
//  JapaneseMyWay
//
//  Created By Ray on 3/9/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KanjiDrawing.h"
#import "KanjiDrawingViewDelegate.h"
#import "KanjiEntry.h"

@interface KanjiDrawingView : UIView {
	CGPoint touchPoint;
	CGPoint previousTouchPoint;
    
	KanjiDrawing *drawing;
    KanjiDrawing *partOfDrawing; // if drawing is part of another drawing, this is the drawing it is part of
	KanjiDrawing *quantizedDrawing;
	
	BOOL isDrawing;
	BOOL drawingEnabled;
    BOOL keepAspect;
    BOOL drawFrame;
    BOOL isStatic;
    
	NSTimer *playTimer;
	BOOL isPlaying;
    int playFrame;
//	int playStroke;
//	int playVector;
	BOOL drawOutline;
    float strokeWidth;
	
    UIColor *drawColor;
    UIColor *shadowColor;
	
	// delegate for callback of stroke count etc
	id<KanjiDrawingViewDelegate> delegate;
}

@property (nonatomic, assign) id<KanjiDrawingViewDelegate> delegate;
@property (nonatomic, retain) KanjiDrawing *drawing;
@property (nonatomic, retain) KanjiDrawing *partOfDrawing;
@property (nonatomic, retain) KanjiDrawing *quantizedDrawing;
@property (nonatomic, retain) NSTimer *playTimer;
@property (nonatomic, retain) UIColor *drawColor;
@property (nonatomic, retain) UIColor *shadowColor;
@property (nonatomic) BOOL isPlaying;
@property (nonatomic) BOOL isStatic;
@property (nonatomic) BOOL isDrawing;
@property (nonatomic) BOOL drawOutline;
@property (nonatomic) BOOL keepAspect;
@property (nonatomic) BOOL drawFrame;
@property (nonatomic) float strokeWidth;
@property (nonatomic, assign) int playFrame;

-(void)initialize;
-(void)initPlay;
-(void)startPlay;
-(void)stopPlay;
-(void)refreshDrawing;
-(NSUInteger)getStrokeCount;
//-(void)drawKanjiOnContext:(CGContextRef)context withMargin:(float)inMargin drawFull:(BOOL)drawFull;
-(void)clearStrokes;
-(void)enableDrawing;
-(void)disableDrawing;
-(void)playTimerFired:(NSTimer*)inTimer;
-(int)getFrameCount;
-(void)drawKanjiOnContext:(CGContextRef)context withMargin:(float)inMargin toFrame:(int)inFrame;

@end
