//
//  BuildKanjiView.m
//  JapaneseMyWay
//
//  Created by Ray on 6/4/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BuildKanjiView.h"
#import "KanjiDrawingView.h"
#import "RadicalSlot.h"
#import "UIViewLayoutAdditions.h"

@implementation BuildKanjiView

@synthesize slots;
@synthesize linkedKanji;
@synthesize linkedKanjiView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)initializeWithRadicals:(NSMutableArray*)inRadicals forKanji:(KanjiEntry*)inKanjiEntry linkToKanjiView:(KanjiDrawingView*)inLinkedKanjiView {
	// initialization code
    self.linkedKanji = inKanjiEntry;
    self.linkedKanjiView = inLinkedKanjiView;
    self.linkedKanjiView.partOfDrawing = inKanjiEntry.drawing;
    if (@available(iOS 13.0, *)) {
        self.linkedKanjiView.drawColor = [UIColor labelColor];
    } else {
        // Fallback on earlier versions
        self.linkedKanjiView.drawColor = [UIColor blackColor];
    }
    self.slots = [NSMutableArray arrayWithCapacity:10];
    
    // remove any existing subviews
    while (self.subviews.count > 0)
        [[self.subviews objectAtIndex:0] removeFromSuperview];
    
    // create slots here based on kanji, do backwards so first stroke is on top
    for (NSInteger i=inRadicals.count-1; i >= 0; i--) {
        // add slot, make it between 0.0 and 1.0 of size of kanji?
        KanjiRadical *thisRadical = [inRadicals objectAtIndex:i];
        [self addSlotForRadical:thisRadical];
    }
    
}

-(BOOL)isSlotFilled:(int)inSlotIndex {
    // slots are backwards ordered
    NSUInteger myIndex = slots.count - inSlotIndex - 1;
    RadicalSlot *thisSlot = [slots objectAtIndex:myIndex];
    BOOL isFilled = NO;
    if (thisSlot.filledRadical != nil)
        isFilled = YES;
    return isFilled;
}

-(BOOL)isRadicalPlaced:(KanjiRadical*)inRadical {
    // see if radical is filled in any slots
    for (int i=0; i < slots.count; i++) {
        RadicalSlot *thisSlot = [slots objectAtIndex:i];
        if (thisSlot.filledRadical == inRadical)
            return YES;
    }
    
    return NO;
}

-(void)fillSlotsWithCorrectRadical {
    // fill all slots with correct radicals
    for (int i=0; i < slots.count; i++) {
        RadicalSlot *thisSlot = [slots objectAtIndex:i];
        thisSlot.filledRadical = thisSlot.expectedRadical;
    }
    
    // refresh drawing
    [self setNeedsDisplay];
}

-(void)clearSlots {
    // clear all slots and subviews
    // MAKE SLOT SUBCLASS OF UIVIEW!!!
    [slots removeAllObjects];
    while (self.subviews.count > 0)
        [[self.subviews objectAtIndex:0] removeFromSuperview];
}

-(void)resetSlots {
    // clear drawing
    [linkedKanjiView.drawing clearStrokes];
     
    // clear all slots of filled radicals
    for (int i=0; i < slots.count; i++) {
        // remove kanji drawing and add label again
        RadicalSlot *thisSlot = [slots objectAtIndex:i];
        thisSlot.drawFrame = YES;
        thisSlot.filledRadical = nil;
    }
}

-(BOOL)isAllFilled {
    // check to see if all slots are filled
    BOOL allFilled = YES;
    for (int i=0; i < slots.count; i++) {
        RadicalSlot *thisSlot = [slots objectAtIndex:i];
        if (thisSlot.filledRadical == nil) {
            allFilled = NO;
            break;
        }
    }
    
    return allFilled;
}

-(BOOL)isAnyFilled {
    // check to see if all slots are filled
    for (int i=0; i < slots.count; i++) {
        RadicalSlot *thisSlot = [slots objectAtIndex:i];
        if (thisSlot.filledRadical != nil)
            return YES;
    }
    
    return NO;
}

-(void)showText {
    // show text for all slots
    for (int i=0; i < slots.count; i++) {
        RadicalSlot *thisSlot = [slots objectAtIndex:i];
        thisSlot.drawText = YES;
    }
}

-(void)hideText {
    // hide text for all slots
    for (int i=0; i < slots.count; i++) {
        RadicalSlot *thisSlot = [slots objectAtIndex:i];
        thisSlot.drawText = NO;
    }
}

-(void)addSlotForRadical:(KanjiRadical*)inRadical {
    /*
    // find frame size
    Rect2 *inBb = inRadical.normalizedBoundingBox;
    
    // this bb is relative to the entire kanji, now modify scale and position to keep aspect ratio
    // based on this frame
    
    // create slot based on frame and normalize bb
    CGRect slotFrame = CGRectMake(self.frame.size.width*inBb.topLeft.x, self.frame.size.height*inBb.topLeft.y, self.frame.size.width*(inBb.bottomRight.x-inBb.topLeft.x), self.frame.size.height*(inBb.bottomRight.y-inBb.topLeft.y));
    */
    
    
    // add slot
    CGRect slotFrame = [self getFrameForRadical:inRadical];
    RadicalSlot *newSlot = [[RadicalSlot alloc] initWithFrame:slotFrame expectedRadical:inRadical];
    newSlot.translatesAutoresizingMaskIntoConstraints = NO;
    [slots addObject:newSlot];
    [self addSubview:newSlot];
    
    // set auto layout to preserve position despite re-sizing
    [self addConstraintsToBeRelativeToParentForView:newSlot];
    
    [newSlot release];
}

-(CGRect)getFrameForRadical:(KanjiRadical*)inRadical {
    // get strokes for radical based on it's drawing and use to calc bounding box
    NSMutableArray *radicalStrokes = [inRadical.drawing getStrokesForFrame:self.bounds asPartOfDrawing:linkedKanji.drawing withMargin:0.0f];
    KanjiDrawing *tempDrawing = [[KanjiDrawing alloc] initWithStrokes:radicalStrokes];
    Rect2 *bb = [tempDrawing getBoundingBox];
    CGRect slotFrame = CGRectMake(bb.topLeft.x, bb.topLeft.y, bb.bottomRight.x-bb.topLeft.x, bb.bottomRight.y-bb.topLeft.y);
    [tempDrawing release];
    
    return slotFrame;
}

-(BOOL)isFilledCorrectly {
    // check to see all slots are filled with expected radical
    BOOL filledCorrectly = YES;
    for (int i=0; i < slots.count; i++) {
        RadicalSlot *thisSlot = [slots objectAtIndex:i];
        if (![thisSlot.expectedRadical.kanji isEqualToString:thisSlot.filledRadical.kanji]) {
            filledCorrectly = NO;
            break;
        }
    }
    
    return filledCorrectly;
}

-(void)didDragRadical:(KanjiRadical*)inRadical toPoint:(CGPoint)inPoint {
    // clear all strokes from linked view, as we're going to re-add
    [linkedKanjiView.drawing clearStrokes];
    [linkedKanjiView.quantizedDrawing clearStrokes];
    
    // if we dragged inside one of the slots
    BOOL slotFound = NO;
    for (NSInteger i=slots.count-1; i >= 0; i--) {
        RadicalSlot *thisSlot = [slots objectAtIndex:i];
        // add strokes for any filled
        if (thisSlot.filledRadical != nil) {
            // translate this radicals strokes to that of the expected radical
            Rect2 *bb = [thisSlot.expectedRadical.drawing getBoundingBox];
            CGRect expectedFrame = CGRectMake(bb.topLeft.x, bb.topLeft.y, bb.bottomRight.x-bb.topLeft.x, bb.bottomRight.y-bb.topLeft.y);
            KanjiDrawing *translatedRadical = [[KanjiDrawing alloc] initWithStrokes:[thisSlot.filledRadical.drawing getStrokesForFrame:expectedFrame asPartOfDrawing:thisSlot.filledRadical.drawing withMargin:0.0f]];
            
            // add this radicals strokes to the kanji drawing and update
            [linkedKanjiView.drawing.strokes addObjectsFromArray:translatedRadical.strokes];
            [translatedRadical release];
        }
        else {
            if (!slotFound && CGRectContainsPoint(thisSlot.frame, inPoint)) {
                // translate this radicals strokes to that of the expected radical
                Rect2 *bb = [thisSlot.expectedRadical.drawing getBoundingBox];
                CGRect expectedFrame = CGRectMake(bb.topLeft.x, bb.topLeft.y, bb.bottomRight.x-bb.topLeft.x, bb.bottomRight.y-bb.topLeft.y);
                KanjiDrawing *translatedRadical = [[KanjiDrawing alloc] initWithStrokes:[inRadical.drawing getStrokesForFrame:expectedFrame asPartOfDrawing:inRadical.drawing withMargin:0.0f]];
                
                // add this radicals strokes to the kanji drawing and update
                [linkedKanjiView.drawing.strokes addObjectsFromArray:translatedRadical.strokes];
                [translatedRadical release];
                
                // set temporary radical
                thisSlot.temporaryRadical = inRadical;
                slotFound = YES;
            }
            else {
                // clear temporary radical
                if (thisSlot.temporaryRadical != nil) {
                    thisSlot.temporaryRadical = nil;
                }
            }
        }
    }
    
    // update linked kanji view
    [linkedKanjiView setNeedsDisplay];
    
    // refresh drawing
    [self setNeedsDisplay];
}

-(BOOL)didLetGoOfRadical:(KanjiRadical*)inRadical atPoint:(CGPoint)inPoint {
    // copy any temporary radicals to filled
    BOOL didFill = NO;
    for (int i=0; i < slots.count; i++) {
        RadicalSlot *thisSlot = [slots objectAtIndex:i];
        if (thisSlot.temporaryRadical != nil) {
            // make temporary radical the filled one
            thisSlot.filledRadical = thisSlot.temporaryRadical;
            thisSlot.temporaryRadical = nil;
            
            // don't draw frame or label anymore
            thisSlot.drawFrame = NO;
            
            // filled
            didFill = YES;
            break;
        }
    }
    
    // refresh drawing
    [self setNeedsDisplay];
    
    // return flag indicating whether it was filled
    return didFill;
}

-(void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    // draw subviews
    for (int i=0; i < self.subviews.count; i++)
        [[self.subviews objectAtIndex:i] setNeedsDisplay];
}

-(void)setFrame:(CGRect)frame {
	// call super
	[super setFrame:frame];
    
    // recalculate slot's frame
    for (int i=0; i < slots.count; i++) {
        RadicalSlot *thisSlot = [slots objectAtIndex:i];
        CGRect slotFrame = [self getFrameForRadical:thisSlot.expectedRadical];
        thisSlot.frame = slotFrame;
    }
}

- (void)dealloc
{
    [slots release];
    [linkedKanji release];
    [linkedKanjiView release];
    [super dealloc];
}

@end
