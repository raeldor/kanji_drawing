//
//  WriteKanjiViewController.h
//  JapaneseMyWay
//
//  Created by Ray Price on 6/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KanjiDrawingView.h"
#import "BuildKanjiView.h"
#import "WriteKanjiViewControllerDelegate.h"
#import "KanjiEntry.h"

// abstract functionality for the writing and hint system so we can use it
// in other projects
@interface WriteKanjiViewController : UIViewController <KanjiDrawingViewDelegate> {
    // controls we are handling
	UILabel *strokesLabel;
	UIButton *clearButton;
	UIButton *helpButton;
	KanjiDrawingView *drawingView;
	BuildKanjiView *buildView;
	KanjiDrawingView *outlineView;
	KanjiDrawingView *animatedView;
    UISlider *animationSlider;

    // drawing state stuff
    int hintDepth;
	BOOL hadToLook;
    
    // temporary views for showing comparison
    KanjiDrawingView *tlView;
    KanjiDrawingView *trView;
    KanjiDrawingView *blView;
    KanjiDrawingView *brView;
    
    // timer to sync two animations
    NSTimer *syncTimer;
    int maxFrame;
    
    // delegate
    id<WriteKanjiViewControllerDelegate> delegate;
    
    // what are we drawing?
    KanjiEntry *targetKanjiEntry;
}

@property (nonatomic, assign) id<WriteKanjiViewControllerDelegate> delegate;
@property (nonatomic, assign) BOOL hadToLook;

-(void)setControllerArray:(NSArray*)inControllers;
-(void)clearButtonPressed:(id)sender;
-(void)clearButtonLongPressed:(id)sender;
-(void)helpButtonPressed:(id)sender;
-(void)resetWithKanjiEntry:(KanjiEntry*)inKanjiEntry andUserDrawing:(KanjiDrawing*)inUserDrawing;
-(void)updateStrokesLeftLabel;
-(void)changeStrokeWidthTo:(int)inStrokeWidth;
-(void)showComparison;
-(void)hideComparison;
-(void)playSyncFrame:(NSTimer*)inTimer;
-(void)sliderChanged:(UISlider*)inSlider;
-(void)setFrameTo:(CGRect)inFrame;
-(void)compareTapped:(id)sender;

@end
