//
//  BuildKanjiView.h
//  JapaneseMyWay
//
//  Created by Ray on 6/4/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KanjiRadical.h"
#import "RadicalDrawingView.h"
#import "KanjiDrawingView.h"

@interface BuildKanjiView : UIView {
    KanjiEntry *linkedKanji;
    KanjiDrawingView *linkedKanjiView; // size of entire view, add dropped radical strokes to this
    NSMutableArray *slots;
}

@property (nonatomic, retain) NSMutableArray *slots;
@property (nonatomic, retain) KanjiEntry *linkedKanji;
@property (nonatomic, retain) KanjiDrawingView *linkedKanjiView;

-(void)initializeWithRadicals:(NSMutableArray*)inRadicals forKanji:(KanjiEntry*)inKanjiEntry linkToKanjiView:(KanjiDrawingView*)inLinkedKanjiView;
-(void)addSlotForRadical:(KanjiRadical*)inRadical;
-(void)didDragRadical:(KanjiRadical*)inRadical toPoint:(CGPoint)inPoint;
-(BOOL)didLetGoOfRadical:(KanjiRadical*)inRadical atPoint:(CGPoint)inPoint;
-(BOOL)isRadicalPlaced:(KanjiRadical*)inRadical;
-(BOOL)isSlotFilled:(int)inSlotIndex;
-(CGRect)getFrameForRadical:(KanjiRadical*)inRadical;
-(void)resetSlots;
-(void)clearSlots;
-(void)fillSlotsWithCorrectRadical;
-(BOOL)isAllFilled;
-(BOOL)isAnyFilled;
-(BOOL)isFilledCorrectly;
-(void)showText;
-(void)hideText;

@end
