//
//  RadicalDrawingView.h
//  JapaneseMyWay
//
//  Created by Ray on 6/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KanjiDrawing.h"
//#import "MyCFPointer.h"
#import "KanjiDrawingView.h"
#import "KanjiRadical.h"
#import "RadicalDrawingViewDelegate.h"

@interface RadicalDrawingView : UIView {
    CGPoint lastPoint;
    KanjiRadical *radical;
    KanjiDrawing *drawing;
	id<RadicalDrawingViewDelegate> delegate;
    CGRect originalFrame;
    
    /*
	KanjiDrawing *drawing;
    
	BOOL drawOutline;
	BOOL drawHalfWidth;
	UIColor *drawColor;*/
}

@property (nonatomic, assign) id<RadicalDrawingViewDelegate> delegate;
@property (nonatomic, retain) KanjiRadical *radical;
@property (nonatomic, retain) KanjiDrawing *drawing;
@property (nonatomic, assign) CGRect originalFrame;

-(void)initializeWithDelegate:(id<RadicalDrawingViewDelegate>)inDelegate;
-(void)changeRadicalTo:(KanjiRadical*)inRadical;

/*
@property (nonatomic, retain) KanjiDrawing *drawing;
@property (nonatomic, retain) UIColor *drawColor;
@property (nonatomic) BOOL drawOutline;
@property (nonatomic) BOOL drawHalfWidth;

-(void)refreshDrawing;
-(void)drawKanji:(CGContextRef)context drawFull:(BOOL)drawFull;
-(void)clearStrokes;
-(void)viewDidLoad;
*/
@end
