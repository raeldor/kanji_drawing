//
//  RadicalDrawingView.m
//  JapaneseMyWay
//
//  Created by Ray on 6/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RadicalDrawingView.h"
#import "Vector2.h"
#import "KanjiStroke.h"
#import "GraphicsHelpers.h"
#import "UIViewLayoutAdditions.h"

@implementation RadicalDrawingView

@synthesize radical;
@synthesize drawing;
@synthesize delegate;
@synthesize originalFrame;

-(void)initializeWithDelegate:(id<RadicalDrawingViewDelegate>)inDelegate {
    // save delegate
    delegate = inDelegate;
    
    // no resizing
    self.translatesAutoresizingMaskIntoConstraints = NO;
    
	// save frame for restore after drag
    originalFrame = self.frame;
}

-(void)awakeFromNib {
    // call super
    [super awakeFromNib];
    
	// set background of view to transparent
	self.backgroundColor = [[UIColor systemBackgroundColor] colorWithAlphaComponent:0.0f];
    
    // no resizing
    self.translatesAutoresizingMaskIntoConstraints = NO;
}

-(void)layoutSubviews {
    // call super
    [super layoutSubviews];
    
    // save frame for restore after drag
    originalFrame = self.frame;
    
    // need to redraw
    [[self.subviews objectAtIndex:0] setNeedsDisplay];
}

-(void)changeRadicalTo:(KanjiRadical*)inRadical {
    // save radical
    self.radical = inRadical;
    
    // remove any existing subviews
    while (self.subviews.count > 0)
        [[self.subviews objectAtIndex:0] removeFromSuperview];
    
    // if radical is not nil
    if (radical != nil) {
        // save drawing
//        self.drawing = inRadical.drawing;
    
        // create a kanji drawing and add as sub view
        KanjiDrawingView *newDrawingView = [[KanjiDrawingView alloc] initWithFrame:self.bounds];
        newDrawingView.translatesAutoresizingMaskIntoConstraints = NO;
        // support dark mode in ios13
        if (@available(iOS 13.0, *)) {
            newDrawingView.drawColor = [UIColor labelColor];
        } else {
            // Fallback on earlier versions
            newDrawingView.drawColor = [UIColor blackColor];
        }
        newDrawingView.shadowColor = [UIColor lightGrayColor];
        newDrawingView.isStatic = YES;
        newDrawingView.userInteractionEnabled = NO;
        newDrawingView.keepAspect = YES;
        newDrawingView.drawFrame = NO;
        newDrawingView.drawing = inRadical.drawing;
        newDrawingView.strokeWidth = 2;
        [self addSubview:newDrawingView];
        [self addConstraintsToFillParentForView:newDrawingView];
        [newDrawingView release];
    }
//    else
//        self.drawing = nil;
}

-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event {
	NSSet *myTouches = [event touchesForView:self];
	UITouch *touch = [myTouches anyObject];
	CGPoint point = [touch locationInView:self.superview];
    
    // save point
    lastPoint = point;
    
    // make the view bigger
    /*
    CGRect myFrame = self.frame;
    CGPoint scaleUp = CGPointMake(self.frame.size.width*0.1f, self.frame.size.height*0.1f);
    myFrame.origin.x -= scaleUp.x/2.0f;
    myFrame.origin.y -= scaleUp.y/2.0f;
    myFrame.size.width += scaleUp.x;
    myFrame.size.height += scaleUp.y;
    self.frame = myFrame;
    */
    
    // need to re-size drawing too
//    [[self.subviews objectAtIndex:0] setNeedsLayout];
//    [[self.subviews objectAtIndex:0] setNeedsDisplay];
}

-(void)touchesMoved:(NSSet*)touches withEvent:(UIEvent*)event {
	NSSet *myTouches = [event touchesForView:self];
	UITouch *touch = [myTouches anyObject];
	CGPoint point = [touch locationInView:self.superview];
    
    // find difference and move by this amount
    CGPoint diff = CGPointMake(point.x-lastPoint.x, point.y-lastPoint.y);
    self.center = CGPointMake(self.center.x+diff.x, self.center.y+diff.y);
    
    // tell delegate that we've moved
    [delegate didMoveRadical:radical toPoint:self.center];

    // save point
    lastPoint = point;
}

-(void)touchesEnded:(NSSet*)touches withEvent:(UIEvent*)event {
    // tell delegate that we've let go
    [delegate didLetGoOfRadical:radical atPoint:self.center];
    
    // restore the frame
    self.frame = originalFrame;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
}

- (void)dealloc {
	// stop playing if drawing
	[radical release];
	[drawing release];
    [super dealloc];
}

@end
