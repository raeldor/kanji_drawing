//
//  RadicalSlot.m
//  JapaneseMyWay
//
//  Created by Ray on 6/4/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RadicalSlot.h"

@implementation RadicalSlot

@synthesize expectedRadical;
@synthesize filledRadical;
@synthesize temporaryRadical;
@synthesize drawFrame;
@synthesize drawText;

-(id)initWithFrame:(CGRect)frame expectedRadical:(KanjiRadical*)inExpectedRadical {
    if ((self = [super initWithFrame:frame])) {
        // save expected radical
        self.expectedRadical = inExpectedRadical;
        
        // draw frame and text by default
        self.drawFrame = YES;
        self.drawText = YES;
        
        // set label layout
//        self.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
        self.numberOfLines = 0;
        self.text = @"";
//        self.text = [radicalKanji getAllMeaningsUsingSeperator:@"\r\n"];
        self.backgroundColor = [[UIColor systemBackgroundColor] colorWithAlphaComponent:0.0f];
        // support dark mode in ios13
        if (@available(iOS 13.0, *)) {
            self.textColor = [UIColor labelColor];
        } else {
            // Fallback on earlier versions
            self.textColor = [UIColor blackColor];
        }
        self.textAlignment = UITextAlignmentCenter;
        self.adjustsFontSizeToFitWidth = YES;
        self.lineBreakMode = UILineBreakModeWordWrap;
//        self.shadowColor = [UIColor grayColor];
//        self.shadowOffset = CGSizeMake(1, 1);
        self.minimumFontSize = 6;
    }
    return self;
}

-(void)drawRect:(CGRect)rect {
    // if we are drawing frame
    if (drawFrame) {
        // get graphics context
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        // save state first
        CGContextSaveGState(context);
        
        // draw frame around this label
        CGContextSetStrokeColorWithColor(context, self.textColor.CGColor);
        CGContextSetLineCap(context, kCGLineCapButt);
        CGContextSetBlendMode(context, kCGBlendModeNormal);
        CGContextSetLineWidth(context, 3.0f);
        CGContextStrokeRect(context, self.bounds);
        
        // restore state again
        CGContextRestoreGState(context);
        
        // draw label
        if (drawText)
            self.text = [expectedRadical.meanings objectAtIndex:0];
        else
            self.text = @"";
        [super drawRect:rect];
    }
}

-(void)dealloc {
	[filledRadical release];
	[expectedRadical release];
	[temporaryRadical release];
	[super dealloc];
}

@end
