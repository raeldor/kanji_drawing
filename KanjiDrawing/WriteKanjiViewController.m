//
//  WriteKanjiViewController.m
//  JapaneseMyWay
//
//  Created by Ray Price on 6/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "WriteKanjiViewController.h"
#import "KanjiStroke.h"
#import <QuartzCore/QuartzCore.h>

#define UIColorFromRGB2(r,g,b) [UIColor colorWithRed:(float)r/255.0f green:(float)g/255.0f blue:(float)b/255.0f alpha:1.0f]

@implementation WriteKanjiViewController

@synthesize delegate;
@synthesize hadToLook;

-(void)setControllerArray:(NSArray*)inControllers {
    self = [super init];
    if (self) {
        // save controllers
        strokesLabel = [[inControllers objectAtIndex:0] retain];
        clearButton = [[inControllers objectAtIndex:1] retain];
        helpButton = [[inControllers objectAtIndex:2] retain];
        drawingView = [[inControllers objectAtIndex:3] retain];
        buildView = [[inControllers objectAtIndex:4] retain];
        outlineView = [[inControllers objectAtIndex:5] retain];
        animatedView = [[inControllers objectAtIndex:6] retain];
        animationSlider = [[inControllers objectAtIndex:7] retain];
        
		// configure drawing views
		outlineView.delegate = self;
        outlineView.isStatic = YES;
		outlineView.drawOutline = YES;
		outlineView.userInteractionEnabled = NO;
		animatedView.delegate = self;
        animatedView.isStatic = YES;
		animatedView.userInteractionEnabled = NO;
		drawingView.delegate = self;
    }
}

-(void)resetWithKanjiEntry:(KanjiEntry*)inKanjiEntry andUserDrawing:(KanjiDrawing*)inUserDrawing {
    // save kanji entry
    if (targetKanjiEntry != nil)
        [targetKanjiEntry release];
    targetKanjiEntry = [inKanjiEntry retain];
    
	// reset card ready for new question
    if (inUserDrawing == nil) {
        [drawingView clearStrokes];
        [drawingView refreshDrawing];
    }
    else {
        NSMutableArray *copyStrokes =[[NSMutableArray alloc] initWithArray:inUserDrawing.strokes copyItems:YES];
        drawingView.drawing.strokes = copyStrokes;
        [copyStrokes release];
    }
    [outlineView clearStrokes];
	[outlineView refreshDrawing];
    [animatedView clearStrokes];
	[animatedView refreshDrawing];
    [buildView clearSlots];
    buildView.opaque = YES;
    buildView.alpha = 1.0f;
	
	// user didn't have to look at answer yet
    hintDepth = 0;
	hadToLook = NO;
}

-(void)changeStrokeWidthTo:(int)inStrokeWidth {
    // change stroke widths on all drawings
    drawingView.strokeWidth = inStrokeWidth;
    outlineView.strokeWidth = inStrokeWidth;
    animatedView.strokeWidth = inStrokeWidth;
}

-(void)clearButtonLongPressed:(id)sender {
	// always stop any animation
	[animatedView stopPlay];
	
	// if we have drawn any strokes, clear them first
	if (drawingView.drawing.strokes.count > 0) {
        // remove all strokes on long press
        [drawingView clearStrokes];
        [drawingView refreshDrawing];
        
        // remove quantized drawing too, otherwise the old version gets left behind
        // this was causing old drawing to be shown in gray when new draw was failing
        drawingView.quantizedDrawing = nil;
	}
    
    // update strokes left
    [self updateStrokesLeftLabel];
}

-(void)clearButtonPressed:(id)sender {
	// always stop any animation
	[animatedView stopPlay];
	
	// if we have drawn any strokes, clear them first
	if (drawingView.drawing.strokes.count > 0) {
        // remove only last stroke
//		[drawingView clearStrokes];
        [drawingView.drawing.strokes removeObjectAtIndex:drawingView.drawing.strokes.count-1];
        [drawingView refreshDrawing];
        
        // remove quantized drawing too, otherwise the old version gets left behind
        // this was causing old drawing to be shown in gray when new draw was failing
        drawingView.quantizedDrawing = nil;
	}
	else {
        // decrease hint depth if we have hints shown
        if (hintDepth > 0) {
            // clear current hint
            switch (hintDepth) {
                case 4: // clear animated drawing
                    [animatedView clearStrokes];
                    [animatedView refreshDrawing];
                    break;
                case 3: // clear outline
                    // clear outline drawing
                    [outlineView clearStrokes];
                    [outlineView refreshDrawing];
                    
                    // reset the opacity on the slots
                    buildView.opaque = YES;
                    buildView.alpha = 1.0f;
                    break;
                case 2: // clear slots and go back to showing whole kanji hint
                    // create a temporary radical array with the entire kanji
                    [buildView clearSlots];
                    NSMutableArray *tempRadicals = [NSMutableArray arrayWithCapacity:10];
                    KanjiRadical *wholeRadical = [[KanjiRadical alloc] initUsingKanjiEntry:targetKanjiEntry];
                    [tempRadicals addObject:wholeRadical];
                    [wholeRadical release];
                    [buildView initializeWithRadicals:tempRadicals forKanji:targetKanjiEntry linkToKanjiView:drawingView];
                    break;
                case 1: // clear all slots
                    [buildView clearSlots];
                    break;
                default:
                    break;
            }
            
            // decrease hint depth
            hintDepth--;
            
            // if the radical count is 1 then skip the next hint because it'll be the same as whole kanji
            if (hintDepth == 2 && targetKanjiEntry.radicals.count == 1)
                hintDepth--;
        }
	}
	
    // update strokes left
    [self updateStrokesLeftLabel];
}

-(void)helpButtonPressed:(id)sender {
	// clear users strokes
	[drawingView.drawing clearStrokes];
	[drawingView refreshDrawing];
    
    // increase hint depth if we still have some to show
    if (hintDepth < 6) {
        // increase hint depth
        hintDepth++;
        
        // if radical count is 1 then skip this hint because it'll be the same as the entire kanji
        if (hintDepth == 1 && targetKanjiEntry.radicals.count == 1)
            hintDepth++;
        
        // show next hint
        switch (hintDepth) {
            case 1: // show whole kanji hint
            {
                // create a temporary radical array with the entire kanji
                NSMutableArray *tempRadicals = [NSMutableArray arrayWithCapacity:10];
                KanjiRadical *wholeRadical = [[KanjiRadical alloc] initUsingKanjiEntry:targetKanjiEntry];
                //                KanjiRadical *wholeRadical = [[KanjiRadical alloc] initUsingDrawing:thisKanji.drawing andMeanings:thisKanji.meanings andKanji:thisKanji.kanji];
                [tempRadicals addObject:wholeRadical];
                [wholeRadical release];
                [buildView initializeWithRadicals:tempRadicals forKanji:targetKanjiEntry linkToKanjiView:drawingView];
            }
                break;
            case 2: // show actual radicals hint
                // clear whole kanji display and show actual radicals for this kanji
                [buildView clearSlots];
                [buildView initializeWithRadicals:targetKanjiEntry.radicals forKanji:targetKanjiEntry linkToKanjiView:drawingView];
                break;
            case 3: // show outline of strokes
            {
                // set outline drawing and refresh
                NSMutableArray *copyStrokes = [targetKanjiEntry.drawing.strokes copy];
                outlineView.drawing.strokes = copyStrokes;
                [copyStrokes release];
                [outlineView refreshDrawing];
                
                // lower opacity of build view
                buildView.opaque = NO;
                buildView.alpha = 0.3f;
            }
                break;
            case 4: // show animated drawing
            {
                // play animation
                // set outline drawing and refresh
                NSMutableArray *copyStrokes = [targetKanjiEntry.drawing.strokes copy];
                animatedView.drawing.strokes = copyStrokes;
                [copyStrokes release];
                [animatedView startPlay];
            }
                break;
            case 5: // show entire strokes and turn card
                // clear outline and animation
                [animatedView stopPlay];
                
                // call delegate
                [delegate writeKanjiSkipped:self];
                
                break;
            default:
                break;
        }
    }
    
	// uh-oh, user had to look!
	hadToLook = YES;
    
    // alert delegate
    [delegate hadToLookWhileDrawingKanji:self];
	
    // refresh
	[outlineView refreshDrawing];
    
    //	[self refreshCard];
    // update strokes left
    [self updateStrokesLeftLabel];
}

-(void)showComparison {
    // show animation of drawing moving to top left
    
    // hide all existing controllers
    buildView.hidden = YES;
    drawingView.hidden = YES;
    outlineView.hidden = YES;
    animatedView.hidden = YES;
    
    // create four kanjidrawing views
    //
    // topleft - real kanji
    // topright - your kanji
    // bottomleft - real animation
    // bottomright - your animation
    //
    // and create a progress bar the user can drag
    
    // calculate view sizes
    float newWidth = outlineView.frame.size.width/2.0f;
    float newHeight = outlineView.frame.size.height/2.0f;
    CGRect tlFrame = CGRectMake(outlineView.frame.origin.x, outlineView.frame.origin.y, newWidth, newHeight);
    CGRect trFrame = CGRectMake(outlineView.frame.origin.x+newWidth, outlineView.frame.origin.y, newWidth, newHeight);
    CGRect blFrame = CGRectMake(outlineView.frame.origin.x, outlineView.frame.origin.y+newHeight, newWidth, newHeight);
    CGRect brFrame = CGRectMake(outlineView.frame.origin.x+newWidth, outlineView.frame.origin.y+newHeight, newWidth, newHeight);
    tlView = [[KanjiDrawingView alloc] initWithFrame:tlFrame];
    trView = [[KanjiDrawingView alloc] initWithFrame:trFrame];
    blView = [[KanjiDrawingView alloc] initWithFrame:blFrame];
    brView = [[KanjiDrawingView alloc] initWithFrame:brFrame];
    tlView.userInteractionEnabled = YES;
    trView.userInteractionEnabled = YES;
    blView.userInteractionEnabled = YES;
    brView.userInteractionEnabled = YES;
    
    // since it's half the size, make stroke width half too
    tlView.strokeWidth = animatedView.strokeWidth/2.0f;
    trView.strokeWidth = animatedView.strokeWidth/2.0f;
    blView.strokeWidth = animatedView.strokeWidth/2.0f;
    brView.strokeWidth = animatedView.strokeWidth/2.0f;

    // add tap recognizer
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(compareTapped:)];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(compareTapped:)];
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(compareTapped:)];
    UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(compareTapped:)];
    [tlView addGestureRecognizer:tap1];
    [trView addGestureRecognizer:tap2];
    [blView addGestureRecognizer:tap3];
    [brView addGestureRecognizer:tap4];
    [tap1 release];
    [tap2 release];
    [tap3 release];
    [tap4 release];
    
    // don't allow user input on these views, otherwise we can't swipe card in JMW
    tlView.userInteractionEnabled = NO;
    trView.userInteractionEnabled = NO;
    blView.userInteractionEnabled = NO;
    brView.userInteractionEnabled = NO;
    
    // match colors of original drawing views
    tlView.drawColor = drawingView.drawColor;
    trView.drawColor = drawingView.drawColor;
    blView.drawColor = drawingView.drawColor;
    brView.drawColor = drawingView.drawColor;
    tlView.shadowColor = drawingView.shadowColor;
    trView.shadowColor = drawingView.shadowColor;
    blView.shadowColor = drawingView.shadowColor;
    brView.shadowColor = drawingView.shadowColor;
    
    // add to same parent as outline view
    UIView *parentView = outlineView.superview;
    [parentView addSubview:tlView];
    [parentView addSubview:trView];
    [parentView addSubview:blView];
    [parentView addSubview:brView];
    
    // set real drawings
    trView.drawing.strokes = [[targetKanjiEntry.drawing.strokes copy] autorelease];
    brView.drawing.strokes = [[targetKanjiEntry.drawing.strokes copy] autorelease];
    tlView.drawing.strokes = [[drawingView.drawing.strokes copy] autorelease];
    blView.drawing.strokes = [[drawingView.drawing.strokes copy] autorelease];
    
    // set border around views
    // set border on drawing view
    [trView.layer setBorderColor: [[UIColor grayColor] CGColor]];
    [trView.layer setBorderWidth: 1.0];
    [brView.layer setBorderColor: [[UIColor grayColor] CGColor]];
    [brView.layer setBorderWidth: 1.0];
    [tlView.layer setBorderColor: [[UIColor grayColor] CGColor]];
    [tlView.layer setBorderWidth: 1.0];
    [blView.layer setBorderColor: [[UIColor grayColor] CGColor]];
    [blView.layer setBorderWidth: 1.0];
    
    //    tlView.backgroundColor = UIColorFromRGB2(255, 51, 51);
    //    blView.backgroundColor = UIColorFromRGB2(255, 102, 102);
    //    trView.backgroundColor = UIColorFromRGB2(102, 255, 102);
    //    brView.backgroundColor = UIColorFromRGB2(51, 255, 51);
    
    // support dark mode in ios13
    if (@available(iOS 13.0, *)) {
        tlView.backgroundColor = [UIColor colorNamed:@"ErrorDrawingColor1"];
        blView.backgroundColor = [UIColor colorNamed:@"ErrorDrawingColor2"];
        trView.backgroundColor = [UIColor colorNamed:@"ErrorDrawingColor2"];
        brView.backgroundColor = [UIColor colorNamed:@"ErrorDrawingColor1"];
    } else {
        // Fallback on earlier versions
        tlView.backgroundColor = UIColorFromRGB2(240, 240, 240);
        blView.backgroundColor = UIColorFromRGB2(220, 220, 220);
        trView.backgroundColor = UIColorFromRGB2(220, 220, 220);
        brView.backgroundColor = UIColorFromRGB2(240, 240, 240);
    }
    
    // set user drawings
    trView.quantizedDrawing = [[[KanjiDrawing alloc] initWithStrokes:[trView.drawing getQuantizedStrokes]] autorelease];
    brView.quantizedDrawing = [[[KanjiDrawing alloc] initWithStrokes:[trView.drawing getQuantizedStrokes]] autorelease];
    tlView.quantizedDrawing = [[[KanjiDrawing alloc] initWithStrokes:[drawingView.drawing getQuantizedNormalizedStrokesSameCountAs:trView.quantizedDrawing.strokes keepAspect:YES]] autorelease];
    blView.quantizedDrawing = [[[KanjiDrawing alloc] initWithStrokes:[drawingView.drawing getQuantizedNormalizedStrokesSameCountAs:trView.quantizedDrawing.strokes keepAspect:YES]] autorelease];
    
    // show close button and slider
    animationSlider.hidden = NO;
    animationSlider.maximumValue = 1.0f;
    if ([blView getFrameCount] > [brView getFrameCount])
        maxFrame = [blView getFrameCount];
    else
        maxFrame = [brView getFrameCount];
    
    // hook into animation slider so user can control animation
    [animationSlider addTarget:self action:@selector(sliderChanged:) forControlEvents:UIControlEventValueChanged];
    
    // compare strokes
    for (int i=0; i < trView.drawing.strokes.count; i++) {
        KanjiStroke *stroke1 = [trView.quantizedDrawing.strokes objectAtIndex:i];
        NSLog(@"stroke vectors %lu\r\n", (unsigned long)stroke1.vectors.count);
    }
    for (int i=0; i < tlView.quantizedDrawing.strokes.count; i++) {
        KanjiStroke *stroke1 = [tlView.quantizedDrawing.strokes objectAtIndex:i];
        NSLog(@"stroke vectors %lu\r\n", (unsigned long)stroke1.vectors.count);
    }
    
    // set as static
    tlView.isStatic = YES;
    trView.isStatic = YES;
    blView.isStatic = YES;
    brView.isStatic = YES;
    
    // have to try and sync play!
    [blView initPlay];
    [brView initPlay];
	syncTimer = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(playSyncFrame:) userInfo:nil repeats:YES];
}

-(void)compareTapped:(id)sender {
    // call delegate
    if (delegate != nil)
        [delegate comparisonDrawingTouched:sender];
}

-(void)setFrameTo:(CGRect)inFrame {
    // set frame for square drawings
    outlineView.frame = inFrame;
    animatedView.frame = inFrame;
    buildView.frame = inFrame;
    
    // calculate view sizes
    float newWidth = outlineView.frame.size.width/2.0f;
    float newHeight = outlineView.frame.size.height/2.0f;
    CGRect tlFrame = CGRectMake(outlineView.frame.origin.x, outlineView.frame.origin.y, newWidth, newHeight);
    CGRect trFrame = CGRectMake(outlineView.frame.origin.x+newWidth, outlineView.frame.origin.y, newWidth, newHeight);
    CGRect blFrame = CGRectMake(outlineView.frame.origin.x, outlineView.frame.origin.y+newHeight, newWidth, newHeight);
    CGRect brFrame = CGRectMake(outlineView.frame.origin.x+newWidth, outlineView.frame.origin.y+newHeight, newWidth, newHeight);
    
    // set side-by-side view
    tlView.frame = tlFrame;
    trView.frame = trFrame;
    blView.frame = blFrame;
    brView.frame = brFrame;
}

-(void)hideComparison {
    // remove views
    [tlView removeFromSuperview];
    [trView removeFromSuperview];
    [blView removeFromSuperview];
    [brView removeFromSuperview];
    
    // re-show original controllers
    drawingView.hidden = NO;
    outlineView.hidden = NO;
    animatedView.hidden = NO;
    buildView.hidden = NO;
    
    // remove event monitor
    [animationSlider removeTarget:self action:@selector(sliderChanged:) forControlEvents:UIControlEventValueChanged];
    
    // hide slider
    animationSlider.hidden = YES;
}

-(void)sliderChanged:(UISlider*)inSlider {
    // calculate new frame
    int newFrame = (float)maxFrame*inSlider.value;
    
    // set frame and refresh
    blView.playFrame = newFrame;
    brView.playFrame = newFrame;
    [blView refreshDrawing];
    [brView refreshDrawing];
}

-(void)playSyncFrame:(NSTimer*)inTimer {
    // play animations together
    [blView playTimerFired:nil];
    [brView playTimerFired:nil];
    
    // update animation slider
    int currentFrame = blView.playFrame;
    if (brView.playFrame > currentFrame)
        currentFrame = brView.playFrame;
    animationSlider.value = (float)currentFrame/(float)maxFrame;
    
    // invalidate timer if we've reached the end
    if (currentFrame > maxFrame) {
        [inTimer invalidate];
    }
}

-(void)didEndDrawingStroke:(id)inDrawingView {
	// have we finished drawing?
	if (targetKanjiEntry.strokeCount == [drawingView getStrokeCount]) {
        // update strokes left
        [self updateStrokesLeftLabel];
		
		// check this character and display message
		double score = [drawingView.drawing compareWithDrawing:targetKanjiEntry.drawing];
        
        // inform delegate of score
		[delegate writeKanjiDrawn:self withScore:score hadToLook:hadToLook];
    }
	else {
        // update strokes left
        [self updateStrokesLeftLabel];
	}
}

-(void)updateStrokesLeftLabel {
    NSInteger strokesLeft = targetKanjiEntry.strokeCount - [drawingView getStrokeCount];
    if (strokesLeft >= 0)
        strokesLabel.text = [NSString stringWithFormat:@"%lu strokes left", (unsigned long)strokesLeft];
}

-(void)dealloc {
    // release our hold on the views
    [strokesLabel release];
    [clearButton release];
    [helpButton release];
    [drawingView release];
    [buildView release];
    [outlineView release];
    [animatedView release];
    [animationSlider release];
    
    // call super
    [super dealloc];
}

@end
